#include <iostream>
#include <string>
#include <cmath>
#include "LexicalAnalyzer.h"

using namespace std;

nsMathParser::LexicalAnalyzer::LexicalAnalyzer()
    : _previousChar(-1), _value(0),
      _column(0), _line(1)
{
}

string nsMathParser::LexicalAnalyzer::tokenToString(Token t)
{
    switch (t)
    {
        case PLUS:
           return "+";
        case MINUS:
           return "-";
        case MULT:
           return "*";
        case DIV:
           return "/";
        case IDENT:
           return "identifier";
        case NUMBER:
           return "number";
        case OPEN_PARENTHESIS:
           return "(";
        case CLOSE_PARENTHESIS:
           return ")";
        case END:
           return "end";
        case ERROR:
        default:
            return "ERREUR";
    }
}

void nsMathParser::LexicalAnalyzer::ungetChar(char c)
{
    _previousChar = c;
    --_column;
}

char nsMathParser::LexicalAnalyzer::getChar()
{
    char r;
    if (_previousChar >= 0)
    {
        r = _previousChar;
        _previousChar = -1;
    }
    else
    {
        r = cin.get();
    }
    ++_column;
    return r;
}

int nsMathParser::LexicalAnalyzer::charToInt (char c)
{
    return c-'0';
}

double nsMathParser::LexicalAnalyzer::getValue()
{
    return _value;
}

nsMathParser::Token nsMathParser::LexicalAnalyzer::nextToken()
{
    _str = "";
    _value = 0;
    char c;
    int nbDec = 0;
    State state = START;
    while (true)
    {
        c = getChar();
        switch (state)
        {
            case START:
                {
                    if (isspace(c))
                    {
                        if (c == '\n')
                        {
                            ++_line;
                            _column = 0;
                        }
                        state = START;
                    }
                    else if (c == '+')
                        return PLUS;
                    else if (c == '-')
                        return MINUS;
                    else if (c == '*')
                        return MULT;
                    else if (c == '/')
                        return DIV;
                    else if (c == '(')
                        return OPEN_PARENTHESIS;
                    else if (c == ')')
                        return CLOSE_PARENTHESIS;
                    else if (isdigit(c))
                    {
                        _value = charToInt(c);
                        state = DIGIT;
                    }
                    else if (isalpha(c))
                    {
                        _str += c;
                        state = ALPHA;
                    }
                    else if (cin.eof())
                        return END;
                    else
                        throw LexicalException ("unknown char");
                }
                break;
            case DIGIT:
                {
                    if (isdigit(c))
                    {
                        _value *= 10;
                        _value += charToInt(c);
                        state = DIGIT;
                    }
                    else if (c == '.')
                    {
                        state = FRAC;
                    }
                    else
                    {
                        ungetChar(c);
                        return NUMBER;
                    }
                }
                break;
            case FRAC:
                {
                    if (isdigit(c))
                    {
                        ++nbDec;
                        _value += double(charToInt(c))/pow(double(10),nbDec);
                    }
                    else if (c == '.')
                        throw LexicalException ("another decimal point in the same number");
                    else
                    {
                        ungetChar(c);
                        return NUMBER;
                    }
                }
                break;
            case ALPHA:
                {
                    if (isalnum(c) || c == '_')
                    {
                        _str += c;
                        state = ALPHA;
                    }
                    else
                    {
                        ungetChar(c);
                        return IDENT;
                    }
                }
                break;
            default:
                return ERROR;
        }
    }
    return ERROR;
}

int nsMathParser::LexicalAnalyzer::getLine()
{
    return _line;
}

int nsMathParser::LexicalAnalyzer::getColumn()
{
    return _column;
}
