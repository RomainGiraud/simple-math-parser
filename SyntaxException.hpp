#ifndef SYNTAXEXCEPTION_HPP
#define SYNTAXEXCEPTION_HPP

inline nsMathParser::SyntaxException::SyntaxException(std::string mess) throw()
    : BaseException(mess) {}

#endif /*SYNTAXEXCEPTION_HPP*/
