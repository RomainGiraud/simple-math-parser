#ifndef NODE_H
#define NODE_H

#include <string>

namespace nsMathParser
{
    class Node
    {
        public:
            Node (Node *left=0, Node *right=0);
            virtual ~Node();
            virtual std::string getString() const;
            virtual std::string toString()  const = 0;
            virtual double      getValue()  const = 0;

        protected:
            Node *l, *r;
    };

    class Addition : public Node
    {
        public:
            Addition(Node *left, Node *right);
            virtual ~Addition() {}
            virtual std::string toString() const;
            virtual double      getValue() const;
    };

    class Subtraction : public Node
    {
        public:
            Subtraction(Node *left, Node *right);
            virtual ~Subtraction() {}
            virtual std::string toString() const;
            virtual double      getValue() const;
    };

    class Multiplication : public Node
    {
        public:
            Multiplication(Node *left, Node *right);
            virtual ~Multiplication() {}
            virtual std::string toString() const;
            virtual double      getValue() const;
    };

    class Division : public Node
    {
        public:
            Division(Node *left, Node *right);
            virtual ~Division() {}
            virtual std::string toString() const;
            virtual double      getValue() const;
    };

    class Number : public Node
    {
        public:
            Number(double v);
            virtual ~Number() {}
            virtual std::string getString() const;
            virtual std::string toString()  const;
            virtual double      getValue()  const;

        private:
            double _value;
    };
}

#include "Node.hpp"

#endif /*NODE_H*/
