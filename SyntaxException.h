#ifndef SYNTAXEXCEPTION_H
#define SYNTAXEXCEPTION_H

#include "BaseException.h"

namespace nsMathParser
{
    class SyntaxException : public BaseException
    {
        public:
            SyntaxException(std::string mess) throw();
            virtual ~SyntaxException() throw() {}
    };
}

#include "SyntaxException.hpp"

#endif /*SYNTAXEXCEPTION_H*/
