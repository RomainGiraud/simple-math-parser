#ifndef LEXICALANALYZER_H
#define LEXICALANALYZER_H

#include "LexicalException.h"

namespace nsMathParser
{
    enum Token { PLUS = 1, MINUS, MULT, DIV,
                 IDENT, NUMBER,
                 OPEN_PARENTHESIS, CLOSE_PARENTHESIS,
                 END, ERROR };
    class LexicalAnalyzer
    {
        public:
            LexicalAnalyzer();
            std::string tokenToString(Token t);
            void ungetChar(char c);
            char getChar();
            int charToInt(char c);
            Token nextToken();
            double getValue();
            int getLine();
            int getColumn();

        private:
            enum State { START = 0, ALPHA, DIGIT, FRAC };
            char _previousChar;
            std::string _str;
            double _value;
            int _column,
                _line;
    };
}

#endif /*LEXICALANALYZER_H*/
