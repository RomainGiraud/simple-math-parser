#ifndef BASEEXCEPTION_H
#define BASEEXCEPTION_H

#include <exception>
#include <string>
#include <iostream>

namespace nsMathParser
{
    class BaseException : public std::exception
    {
        public:
            BaseException(std::string mess) throw();
            virtual ~BaseException() throw() {}
            virtual const char* what() const throw();
            virtual std::string getMessage() const throw();

        protected:
            std::string _message;
        
    };
}

std::ostream& operator << (std::ostream& os,
        const nsMathParser::BaseException &ex);

#include "BaseException.hpp"

#endif /*BASEEXCEPTION_H*/
