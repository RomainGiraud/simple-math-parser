BASEEXCEPTION_H    = BaseException.h BaseException.hpp
LEXICALEXCEPTION_H = LexicalException.h LexicalException.hpp $(BASEEXCEPTION_H)
SYNTAXEXCEPTION_H  = SyntaxException.h SyntaxException.hpp $(BASEEXCEPTION_H)
ALL_EXCEPTIONS_H   = $(BASEEXCEPTION_H) $(LEXICALANALYZER_H) $(SYNTAXANALYZER_H)
NODE_H             = Node.h Node.hpp
LEXICALANALYZER_H  = LexicalAnalyzer.h $(LEXICALEXCEPTION_H)
SYNTAXANALYZER_H   = SyntaxAnalyzer.h $(LEXICALANALYZER_H) $(NODE_H) \
					 $(SYNTAXEXCEPTION_H)

OPTIONS= -Wall -Werror -ggdb
COMPILER = g++ -o $@ -c $< $(OPTIONS)

main: main.o LexicalAnalyzer.o SyntaxAnalyzer.o
	g++ -o main $^

main.o: main.cpp $(SYNTAXANALYZER_H) $(ALL_EXCEPTIONS_H)
	$(COMPILER)

LexicalAnalyzer.o: LexicalAnalyzer.cpp $(LEXICALANALYZER_H)
	$(COMPILER)

SyntaxAnalyzer.o: SyntaxAnalyzer.cpp $(SYNTAXANALYZER_H)
	$(COMPILER)

.PHONY: clean

clean:
	rm *.o main -fv
