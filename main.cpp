#include <iostream>
#include <string>

#include "SyntaxAnalyzer.h"
#include "BaseException.h"
#include "LexicalException.h"
#include "SyntaxException.h"

using namespace std;
using namespace nsMathParser;

int main (int argc, char *argv[])
{
    SyntaxAnalyzer analyzer;
    try
    {
        Node *result = analyzer.getTree();
        cout << "Analyze OK" << endl
             << "String : " << result->getString() << endl
             << "Value : " << result->getValue() << endl;
    }
    catch (const LexicalException &error)
    {
        cerr << "[error => lexical analyzer] (" << analyzer.getLine()
             << ',' << analyzer.getColumn() << ") : "
             << error << endl;
        return 1;
    }
    catch (const SyntaxException &error)
    {
        cerr << "[error => syntax analyzer] (" << analyzer.getLine()
             << ',' << analyzer.getColumn() << ") : "
             << error << endl;
        return 1;
    }
    catch (const BaseException &error)
    {
        cerr << "[error => basic] : "
             << error << endl;
        return 1;
    }
    catch (const exception &error)
    {
        cerr << "[error => unknown] : "
             << error.what() << endl;
        return 1;
    }

    return 0;
}
