#include <string>
#include "SyntaxAnalyzer.h"

using namespace std;

nsMathParser::SyntaxAnalyzer::SyntaxAnalyzer()
{
}

nsMathParser::Node* nsMathParser::SyntaxAnalyzer::getTree()
{
    readToken();
    Node* n = expression();
    if (_currentToken == END)
        return n;
    else
        throw SyntaxException ("missing or unexpected character");
}

void nsMathParser::SyntaxAnalyzer::readToken()
{
    _currentToken = _lex.nextToken();
}

void nsMathParser::SyntaxAnalyzer::terminal(Token t)
{
    if (_currentToken == t)
        readToken();
    else
        throw SyntaxException ("'"+_lex.tokenToString(t)+"' expected");
}

nsMathParser::Node* nsMathParser::SyntaxAnalyzer::expression()
{
    Node* n1 = term();
    if (_currentToken == PLUS || _currentToken == MINUS)
    {
        Token t = _currentToken;
        readToken();
        Node* n2 = expression();
        if (t == PLUS)
            return new Addition(n1,n2);
        else
            return new Subtraction(n1,n2);
    }
    else
        return n1;
}

nsMathParser::Node* nsMathParser::SyntaxAnalyzer::term()
{
    Node *n1 = factor();
    if (_currentToken == MULT || _currentToken == DIV)
    {
        Token t = _currentToken;
        readToken();
        Node* n2 = term();
        if (t == MULT)
            return new Multiplication(n1,n2);
        else
            return new Division(n1,n2);
    }
    else
        return n1;
}

nsMathParser::Node* nsMathParser::SyntaxAnalyzer::factor()
{
    int neg = 1;
    if (_currentToken == PLUS)
        readToken();
    if (_currentToken == MINUS)
    {
        readToken();
        neg = -1;
    }
    if (_currentToken == NUMBER)
    {
        Node *n = new Number(_lex.getValue()*neg);
        readToken();
        return n;
    }
    else if (_currentToken == IDENT)
    {
        Node *n = new Number(_lex.getValue()*neg);
        readToken();
        return n;
    }
    else if (_currentToken == OPEN_PARENTHESIS)
    {
        readToken();
        Node *n = expression();
        terminal(CLOSE_PARENTHESIS);
        return n;
    }
    else
        throw SyntaxException ("number, identificator or '(' expected");
}

int nsMathParser::SyntaxAnalyzer::getLine()
{
    return _lex.getLine();
}

int nsMathParser::SyntaxAnalyzer::getColumn()
{
    return _lex.getColumn();
}
