#ifndef SYNTAXANALYZER_H
#define SYNTAXANALYZER_H

#include "LexicalAnalyzer.h"
#include "Node.h"
#include "SyntaxException.h"

namespace nsMathParser
{
    class SyntaxAnalyzer
    {
        public:
            SyntaxAnalyzer();
            Node* getTree();
            int getLine();
            int getColumn();

        private:
            LexicalAnalyzer _lex;
            Token _currentToken;
            void readToken();
            Node* expression();
            Node* term();
            Node* factor();
            void terminal(Token t);
    };
}

#endif /*SYNTAXANALYZER_H*/
