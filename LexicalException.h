#ifndef LEXICALEXCEPTION_H
#define LEXICALEXCEPTION_H

#include "BaseException.h"

namespace nsMathParser
{
    class LexicalException : public BaseException
    {
        public:
            LexicalException(std::string mess) throw();
            virtual ~LexicalException() throw() {}
    };
}

#include "LexicalException.hpp"

#endif /*LEXICALEXCEPTION_H*/
