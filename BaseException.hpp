#ifndef BASEEXCEPTION_HPP
#define BASEEXCEPTION_HPP

inline nsMathParser::BaseException::BaseException(std::string mess) throw()
    : _message(mess) {}

inline std::string nsMathParser::BaseException::getMessage() const throw()
{
    return _message;
}

inline const char* nsMathParser::BaseException::what() const throw()
{
    return _message.c_str();
}

inline std::ostream& operator << (std::ostream& os,
        const nsMathParser::BaseException &ex)
{
    return os << ex.getMessage();
}

#endif /*BASEEXCEPTION_HPP*/
