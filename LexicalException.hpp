#ifndef LEXICALEXCEPTION_HPP
#define LEXICALEXCEPTION_HPP

inline nsMathParser::LexicalException::LexicalException(std::string mess) throw()
    : BaseException(mess) {}

#endif /*LEXICALEXCEPTION_HPP*/
