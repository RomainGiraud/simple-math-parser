#ifndef NODE_HPP
#define NODE_HPP

#include <sstream>

inline nsMathParser::Node::Node (Node *left, Node *right)
    : l(left), r(right) {}
inline nsMathParser::Node::~Node() { delete r; delete l; }
inline std::string nsMathParser::Node::getString() const
{
    return "("+l->getString()+toString()+r->getString()+")";
}

inline nsMathParser::Addition::Addition(Node *left, Node *right)
    : Node(left,right) {}
inline double nsMathParser::Addition::getValue() const
{
    return l->getValue() + r->getValue();
}
inline std::string nsMathParser::Addition::toString() const
{
    return "+";
}

inline nsMathParser::Subtraction::Subtraction(Node *left, Node *right)
    : Node(left,right) {}
inline double nsMathParser::Subtraction::getValue() const
{
    return l->getValue() - r->getValue();
}
inline std::string nsMathParser::Subtraction::toString() const
{
    return "-";
}

inline nsMathParser::Multiplication::Multiplication(Node *left, Node *right)
    : Node(left,right) {}
inline double nsMathParser::Multiplication::getValue() const
{
    return l->getValue() * r->getValue();
}
inline std::string nsMathParser::Multiplication::toString() const
{
    return "*";
}

inline nsMathParser::Division::Division(Node *left, Node *right)
    : Node(left,right) {}
inline double nsMathParser::Division::getValue() const
{
    return l->getValue() / r->getValue();
}
inline std::string nsMathParser::Division::toString() const
{
    return "/";
}

inline nsMathParser::Number::Number(double v)
    : _value(v) {}
inline double nsMathParser::Number::getValue() const
{
    return _value;
}
inline std::string nsMathParser::Number::getString() const
{
    return toString();
}
inline std::string nsMathParser::Number::toString() const
{
    std::stringstream ss;
    ss << _value;
    return ss.str();
}

#endif /*NODE_HPP*/
